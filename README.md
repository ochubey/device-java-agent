## Device Java Agent

This library designed to get Appium server url of free device and release it after usage of the Appium Driver.
Implementation is currently limited to lookup by platform name (iOS/Android) and working with single instance of the Device Dealer (see jar file with instructions in releases).

In order to establish connection between Device Dealer and Appium Driver, please, use following steps:
1. add latest [device-java-agent] dependency to your build file;
2. use code bellow (snippet#1) to book available device and get Appium Server URL before Driver initialization.

[![](https://jitpack.io/v/org.bitbucket.ochubey/device-java-agent.svg)](https://jitpack.io/#org.bitbucket.ochubey/device-java-agent)  [![Codacy Badge](https://api.codacy.com/project/badge/Grade/6d1b578717e6411f83c6c84d1f54133a)](https://www.codacy.com/app/chubej/Device-Java-Agent?utm_source=ochubey@bitbucket.org&amp;utm_medium=referral&amp;utm_content=ochubey/device-java-agent&amp;utm_campaign=Badge_Grade)

## Code snippets
Snippet#1: by default Device Dealer will be executed on localhost:8989 and agent will check for free device 360 times every 10 seconds and if device would not be found - nothing would be returned.
Both host and lookup strategy could be changed based on infrastructure needs in further releases.

```java
public class AppiumTest{
    private static Device device;
    private static DeviceDealerAgent deviceDealerAgent = new DeviceDealerAgent();
    
    private void initDriver(String platform) {
        DesiredCapabilities desiredCapabilities = DEVICE_CAPABILITIES.getDesiredCapabilities();
        device = deviceDealerAgent.bookDevice(platform);
        assertNotNull("Unable to find free device after 60 minutes or Device Dealer is unreachable", device);
    
        if (platform.equalsIgnoreCase("ios")) {
            driver = new IOSDriver(device.getAppiumServerUrl(), desiredCapabilities) {};
        } else if (platform.equalsIgnoreCase("android")) {
            driver = new AndroidDriver(device.getAppiumServerUrl(), desiredCapabilities) {};
        }
        assert driver != null;
    }
}
```

In case if non-defaults settings are needed - both service location and pooling frequency/timeout could be changed:
```java
public class AppiumTest{
    //Agent will try to communicate with service with endpoints on 192.168.1.1:9999
    DeviceDealerAgent deviceDealerAgent = new DeviceDealerAgent("192.168.1.1", 9999);
    
    //Agent will try to get idle device 10 times every 10 seconds
    private void initDriver(String platform) {
        deviceDealerAgent.bookDevice(platform, 10, 60000);
    }
}
``` 

[device-java-agent]:https://jitpack.io/#org.bitbucket.ochubey/device-java-agent
