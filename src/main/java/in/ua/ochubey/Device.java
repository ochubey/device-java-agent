package in.ua.ochubey;

import in.ua.ochubey.api.device.pojo.book.DeviceSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

public class Device {

    private static final Logger logger = LoggerFactory.getLogger("DeviceDealer java agent");

    private String deviceUdid;
    private String deviceName;
    private String deviceVersion;
    private URL appiumServerUrl;
    private Integer serverPort;
    private Integer driverPort;
    private Integer webPort;

    public String getDeviceUdid() {
        return deviceUdid;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public Integer getDriverPort() {
        return driverPort;
    }

    public Integer getWebPort() {
        return webPort;
    }

    public URL getAppiumServerUrl() {
        return appiumServerUrl;
    }

    public Device(DeviceSchema deviceSchema, String host) {
        this.deviceUdid = deviceSchema.getUdid();
        this.deviceName = deviceSchema.getDeviceName();
        this.deviceVersion = deviceSchema.getPlatformVersion();
        this.serverPort = deviceSchema.getServerPort();
        this.driverPort = deviceSchema.getDriverPort();
        this.webPort = deviceSchema.getWebPort();
        Integer appiumPort = deviceSchema.getServerPort();
        try {
            this.appiumServerUrl = new URL(String.format("http://%s:%s/wd/hub/", host, appiumPort));
        } catch (MalformedURLException e) {
            logger.trace(e.getMessage(), e);
        }
    }
}
