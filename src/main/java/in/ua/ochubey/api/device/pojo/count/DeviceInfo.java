package in.ua.ochubey.api.device.pojo.count;

import com.google.gson.annotations.SerializedName;

public class DeviceInfo {
    @SerializedName("deviceName")
    public String deviceName;
    @SerializedName("platformVersion")
    public String platformVersion;
    @SerializedName("deviceStatus")
    public String deviceStatus;
}
