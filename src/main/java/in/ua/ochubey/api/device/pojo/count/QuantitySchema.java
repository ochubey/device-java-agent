package in.ua.ochubey.api.device.pojo.count;

import com.google.gson.annotations.SerializedName;

public class QuantitySchema {
    @SerializedName("_embedded")
    private Embedded embedded;

    public Embedded getEmbedded() {
        return embedded;
    }
}
