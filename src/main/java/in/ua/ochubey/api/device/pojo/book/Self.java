package in.ua.ochubey.api.device.pojo.book;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Self {
  @SerializedName("href")
  @Expose
  private String href;
  public Self(){
  }
  public Self(String href){
   this.href=href;
  }
  public void setHref(String href){
   this.href=href;
  }
  public String getHref(){
   return href;
  }
}