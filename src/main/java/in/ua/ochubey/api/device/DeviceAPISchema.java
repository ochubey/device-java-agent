package in.ua.ochubey.api.device;

import in.ua.ochubey.api.ApiFactory;
import in.ua.ochubey.api.device.pojo.book.DeviceSchema;
import org.apache.http.util.TextUtils;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

/**
 * Created by o.chubey on 5/18/16.
 */
public class DeviceAPISchema {

    public DeviceSchema getDevicesSchemaByPlatform(String apiUrl, String platform) throws IOException {
        if (TextUtils.isEmpty(platform)) {
            return null;
        }
        String byPlatformUrl = "devices/search/bookByPlatform?platform=";
        Call<DeviceSchema> call = ApiFactory.getApi(apiUrl).devicesSchema(byPlatformUrl + platform);
        Response<DeviceSchema> response = call.execute();
        if (response.isSuccessful()) {
            return response.body();
        }
        return null;
    }

}
