package in.ua.ochubey.api.device.pojo.book;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Links {
  @SerializedName("self")
  private Self self;
  @SerializedName("device")
  private LinksDevice device;
  public Links(){
  }
  public Links(Self self, LinksDevice device){
   this.self=self;
   this.device=device;
  }
  public void setSelf(Self self){
   this.self=self;
  }
  public Self getSelf(){
   return self;
  }
  public void setDevice(LinksDevice device){
   this.device=device;
  }
  public LinksDevice getDevice(){
   return device;
  }
}