package in.ua.ochubey.api.device;

import in.ua.ochubey.api.ApiFactory;
import in.ua.ochubey.api.device.pojo.message.DeviceStatusMessage;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

public class IncomingWebHookApiSchema {
    private static final Logger logger = LoggerFactory.getLogger("DeviceDealer java agent > Sending message");

    public void getQuantitySchemaByPlatform(String apiUrl, DeviceStatusMessage json) {
        if (TextUtils.isEmpty(apiUrl)) {
            return;
        }
        Call<String> call = ApiFactory.getApi(apiUrl).sendMessage(apiUrl, json);
        Response<String> response;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
                if (response.body().equalsIgnoreCase("1")) {
                    logger.info("Message sent successfully");
                } else {
                    logger.error(response.body());
                }
            }
        } catch (IOException e) {
            logger.trace(e.getMessage(), e);
        }
    }
}
