package in.ua.ochubey.api.device.pojo.message;

import java.util.List;

public class DeviceStatusMessage {

    private String text;
    private String themeColor;
    private String title;
    private List<Section> sections = null;

    public DeviceStatusMessage(String text, String themeColor, String title, List<Section> sections) {
        this.sections = sections;
        this.text = text;
        this.themeColor = themeColor;
        this.title = title;
    }

}
