package in.ua.ochubey.api.device;

import in.ua.ochubey.api.ApiFactory;
import in.ua.ochubey.api.device.pojo.count.QuantitySchema;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

public class QuantityApiSchema {
    private static final Logger logger = LoggerFactory.getLogger("DeviceDealer java agent > Getting available devices");

    public QuantitySchema getQuantitySchemaByPlatform(String apiUrl, String platform) {
        if (TextUtils.isEmpty(platform)) {
            return null;
        }
        String byPlatformUrl = "devices/search/findAllConnectedByPlatform?platform=";
        Call<QuantitySchema> call = ApiFactory.getApi(apiUrl).quantitySchema(byPlatformUrl + platform);
        Response<QuantitySchema> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            }
        } catch (IOException e) {
            logger.trace(e.getMessage(), e);
            return null;
        }

        return null;
    }

}
