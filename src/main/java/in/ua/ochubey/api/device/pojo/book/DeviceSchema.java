package in.ua.ochubey.api.device.pojo.book;

import com.google.gson.annotations.SerializedName;

public class DeviceSchema {
    @SerializedName("driverPort")
    private Integer driverPort;
    @SerializedName("platformVersion")
    private String platformVersion;
    @SerializedName("udid")
    private String udid;
    @SerializedName("serverPort")
    private Integer serverPort;
    @SerializedName("deviceName")
    private String deviceName;
    @SerializedName("platform")
    private String platform;
    @SerializedName("webPort")
    private Integer webPort;

    public DeviceSchema() {
    }

    public DeviceSchema(Integer driverPort, String platformVersion, String udid, Integer serverPort, String deviceName, String platform, Integer
            webPort) {
        setDriverPort(driverPort);
        setPlatformVersion(platformVersion);
        setUdid(udid);
        setServerPort(serverPort);
        setDeviceName(deviceName);
        setPlatform(platform);
        setWebPort(webPort);
    }

    public Integer getDriverPort() {
        return driverPort;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public String getUdid() {
        return udid;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getPlatform() {
        return platform;
    }

    public Integer getWebPort() {
        return webPort;
    }

    private void setDriverPort(Integer driverPort) {
        this.driverPort = driverPort;
    }

    private void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    private void setUdid(String udid) {
        this.udid = udid;
    }

    private void setServerPort(Integer serverPort) {
        this.serverPort = serverPort;
    }

    private void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    private void setPlatform(String platform) {
        this.platform = platform;
    }

    private void setWebPort(Integer webPort) {
        this.webPort = webPort;
    }

    @Override
    public String toString() {
        return "platform=" + platform + ", deviceName=" + deviceName + ", udid=" + udid + ", platformVersion=" + platformVersion;
    }
}