package in.ua.ochubey.api.device.pojo.message;

import java.util.List;

public class Section {

    private String activityTitle = "<br>DEVICE DEALER";
    private String activityImage;
    private List<Fact> facts = null;

    public Section(String activityImage, List<Fact> facts) {
        this.activityImage = activityImage;
        this.facts = facts;
    }

}
