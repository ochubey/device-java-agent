package in.ua.ochubey.api.device.pojo.message;

import in.ua.ochubey.api.device.pojo.count.DeviceInfo;

import java.util.List;

public class Fact {
    private String name;
    private String value;

    public Fact(String name, List<DeviceInfo> deviceInfoList) {
        this.name = name;
        setValue(deviceInfoList);
    }

    private void setValue(List<DeviceInfo> devicesInfo) {
        String htmlTableFormat = "<table>%s</table>";
        String rowFromat = "<tr><td style=\"padding:5px;\">%s</td><td style=\"padding:5px;\">%s</td><td style=\"padding:5px;\">%s</td></tr>";

        StringBuilder deviceInfoAsHtml = new StringBuilder();
        for (DeviceInfo deviceInfo : devicesInfo) {
            String format = String.format(rowFromat, deviceInfo.deviceName, deviceInfo.platformVersion, deviceInfo.deviceStatus);
            deviceInfoAsHtml.append(format);
        }

        this.value = String.format(htmlTableFormat, deviceInfoAsHtml);
    }
}
