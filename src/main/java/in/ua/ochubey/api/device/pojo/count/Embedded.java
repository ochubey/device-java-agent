package in.ua.ochubey.api.device.pojo.count;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Embedded {
    @SerializedName("devices")
    private List<DeviceInfo> devices = null;

    public List<DeviceInfo> getDevices() {
        return devices;
    }
}
