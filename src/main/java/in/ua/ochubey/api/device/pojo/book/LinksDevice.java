package in.ua.ochubey.api.device.pojo.book;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class LinksDevice {
  @SerializedName("href")
  @Expose
  private String href;
  public LinksDevice(){
  }
  public LinksDevice(String href){
   this.href=href;
  }
  public void setHref(String href){
   this.href=href;
  }
  public String getHref(){
   return href;
  }
}