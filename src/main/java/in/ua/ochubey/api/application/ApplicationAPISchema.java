package in.ua.ochubey.api.application;

import in.ua.ochubey.api.ApiFactory;
import org.apache.http.util.TextUtils;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

import static in.ua.ochubey.DeviceDealerAgent.ANDROID;
import static in.ua.ochubey.DeviceDealerAgent.IOS;

public class ApplicationAPISchema {

    public String getAppPath(String apiUrl, String platform, String appId, String token) throws IOException {
        if (TextUtils.isEmpty(platform)) {
            return null;
        }
        String url;
        if (platform.equalsIgnoreCase(IOS)) {
            url = String.format("applications/search/getIosAppPath?appId=%s&hockeyAppToken=%s", appId, token);
        } else if (platform.equalsIgnoreCase(ANDROID)) {
            url = String.format("applications/search/getAndroidAppPath?appId=%s&hockeyAppToken=%s", appId, token);
        } else {
            String errorMessage = String.format("Unsupported platform defined %s. Supported platforms are: %s and %s", platform, IOS, ANDROID);
            throw new IllegalStateException(errorMessage);
        }
        Call<String> call = ApiFactory.getApi(apiUrl).applicationSchema(url);
        Response<String> response = call.execute();
        if (response.isSuccessful()) {
            return response.body();
        }
        return null;
    }

}
