package in.ua.ochubey.api;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by o.chubey on 3/27/17.
 */
public class ApiFactory {

    //waiting for 10 minutes maximum for the device or application
    private static final int TIMEOUT = 600;
    private static ApiService api;

    private ApiFactory() {
        throw new IllegalStateException("Utility class");
    }

    public static ApiService getApi(String apiUrl) {
        if (api == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(apiUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHttpClient())
                    .build();

            api = retrofit.create(ApiService.class);
        }
        return api;
    }

    private static OkHttpClient getHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    HttpUrl originalHttpUrl = original.url();
                    HttpUrl url = originalHttpUrl
                            .newBuilder()
                            .build();
                    Request.Builder requestBuilder = original.newBuilder().url(url);
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                });
        return builder.build();
    }
}
