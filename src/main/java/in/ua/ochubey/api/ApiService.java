package in.ua.ochubey.api;

import in.ua.ochubey.api.device.pojo.book.DeviceSchema;
import in.ua.ochubey.api.device.pojo.count.QuantitySchema;
import in.ua.ochubey.api.device.pojo.message.DeviceStatusMessage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by o.chubey on 3/27/17.
 */
public interface ApiService {

    @GET
    Call<DeviceSchema> devicesSchema(
            @Url String url);

    @GET
    Call<String> applicationSchema(
            @Url String url);

    @GET
    Call<QuantitySchema> quantitySchema(
            @Url String ur);

    @POST
    Call<String> sendMessage(
            @Url String url,
            @Body DeviceStatusMessage body);

}
