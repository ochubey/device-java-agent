package in.ua.ochubey;

import in.ua.ochubey.api.application.ApplicationAPISchema;
import in.ua.ochubey.api.device.DeviceAPISchema;
import in.ua.ochubey.api.device.IncomingWebHookApiSchema;
import in.ua.ochubey.api.device.QuantityApiSchema;
import in.ua.ochubey.api.device.pojo.book.DeviceSchema;
import in.ua.ochubey.api.device.pojo.count.DeviceInfo;
import in.ua.ochubey.api.device.pojo.count.Embedded;
import in.ua.ochubey.api.device.pojo.count.QuantitySchema;
import in.ua.ochubey.api.device.pojo.message.DeviceStatusMessage;
import in.ua.ochubey.api.device.pojo.message.Fact;
import in.ua.ochubey.api.device.pojo.message.Section;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

public class DeviceDealerAgent {

    private final ReentrantLock lock = new ReentrantLock();
    private static final Logger logger = LoggerFactory.getLogger("DeviceDealer java agent");

    public static final String IOS = "IOS";
    public static final String ANDROID = "ANDROID";

    private String host;
    private int port;
    private int defaultRetries = 360;
    private int defaultWaitMs = 10000;

    private String apiUrl;

    public DeviceDealerAgent() {
        this.host = "localhost";
        this.port = 8989;
        generateApiUrl(host, port);
    }

    public DeviceDealerAgent(String host, int port) {
        this.host = host;
        this.port = port;
        generateApiUrl(host, port);
    }

    private void generateApiUrl(String host, int port) {
        if (!pingHost(host, port)) {
            String faultMessage = String.format("There is no Device Dealer available by address: http://%s:%s ", host, port);
            throw new IllegalStateException(faultMessage);
        }
        this.apiUrl = String.format("http://%s:%s/", host, port);
    }

    public Device bookDevice(String platform) {
        return bookDevice(platform, defaultRetries, defaultWaitMs);
    }

    public String getApplicationPath(String platform, String appId, String hockeyAppToken) {
        return getApplicationPath(platform, appId, hockeyAppToken, defaultRetries, defaultWaitMs);
    }

    public String getApplicationPath(String platform, String appId, String hockeyAppToken, int retries, int waitMs) {
        ApplicationAPISchema applicationAPISchema;

        int retry = 0;
        int timeOut = defaultRetries * defaultWaitMs;
        long timeOutMinutes = TimeUnit.MILLISECONDS.toMinutes(timeOut);
        while (retry < retries) {
            retry++;
            lock.lock();
            String appPath = null;
            try {
                applicationAPISchema = new ApplicationAPISchema();
                appPath = applicationAPISchema.getAppPath(apiUrl, platform, appId, hockeyAppToken);
            } catch (IOException ignore) {
            }
            lock.unlock();
            if (appPath != null) {
                logger.info("Application found and downloaded to: {}", appPath);
                return appPath;
            } else {
                doSleep(waitMs);
                logger.info("Application was not downloaded, trying again");
            }
        }
        logger.error("Failed to download application after {} minutes", timeOutMinutes);
        return "";
    }

    public Device bookDevice(String platform, int retries, int waitMs) {
        DeviceSchema deviceSchema;

        int timeOut = defaultRetries * defaultWaitMs;
        long timeOutMinutes = TimeUnit.MILLISECONDS.toMinutes(timeOut);
        if (platform.equalsIgnoreCase(IOS) || platform.equalsIgnoreCase(ANDROID)) {
            logger.warn("Starting pooling for {} device during {} minutes with interval of {} milliseconds", platform, timeOutMinutes, defaultWaitMs);
            int retry = 0;
            while (retry < retries) {
                retry++;
                lock.lock();
                DeviceAPISchema deviceAPISchema = new DeviceAPISchema();
                try {
                    deviceSchema = deviceAPISchema.getDevicesSchemaByPlatform(apiUrl, platform);
                } catch (IOException ignore) {
                    deviceSchema = null;
                }
                lock.unlock();
                if (deviceSchema != null) {
                    String foundDeviceMsg = deviceSchema.toString();
                    logger.info("Device found: {}", foundDeviceMsg);
                    return new Device(deviceSchema, host);
                } else {
                    doSleep(waitMs);
                    logger.info("Device was not found, trying again");
                }
            }
        } else {
            logger.error("Unsupported platform defined {}. Supported platforms are: {} and {}", platform, IOS, ANDROID);
        }
        logger.error("Failed to find free device after {} minutes", timeOutMinutes);
        return null;
    }

    public boolean isEnoughDevices(int expectedQuantity, String incomingWebHookUrl, String platform, String message) {
        if (expectedQuantity < 1) {
            logger.error("Incorrect parameters: expectedQuantity should be greater then 0.");
            return false;
        }

        List<DeviceInfo> devicesInfo = getDevicesInfo(platform);
        logger.info(String.valueOf(devicesInfo.size()));

        if (devicesInfo.isEmpty()) {
            logger.error("Empty list of connected devices was returned. Connect devices or check if agent poined onto correct server: {}", apiUrl);
            return false;
        }

        int idle = countIdleDevices(devicesInfo);

        if (!TextUtils.isEmpty(incomingWebHookUrl)) {
            generateMessage(expectedQuantity, idle, platform, incomingWebHookUrl, devicesInfo, message);
        }

        return idle >= expectedQuantity;
    }

    private List<DeviceInfo> getDevicesInfo(String platform) {
        List<DeviceInfo> devicesInfo = Collections.emptyList();

        QuantityApiSchema quantityApiSchema = new QuantityApiSchema();
        QuantitySchema quantitySchema = quantityApiSchema.getQuantitySchemaByPlatform(apiUrl, platform);
        if (quantitySchema == null) {
            logger.error("Unable to receive number of devices. Please make sure Device Dealer is running and accessible by {}", apiUrl);
            return devicesInfo;
        }

        Embedded embedded = quantitySchema.getEmbedded();
        if (embedded == null) {
            logger.error("Device Dealer found, but incorrect response received. Minimum supported version: 0.5.0");
            return devicesInfo;
        }

        return embedded.getDevices();
    }

    private void generateMessage(int expectedQuantity, int idle, String platform, String incomingWebHookUrl, List<DeviceInfo> devicesInfo,
                                 String externalMsg) {
        if (idle < expectedQuantity) {
            if (idle == 0) {
                doSendMessage(platform, devicesInfo, incomingWebHookUrl, externalMsg,
                        "<font color=\"Tomato\"><b>Failed to start</b></font>. " +
                                "<br>Not enough devices found. " +
                                "<br>Make sure no other tests are running and interrupted test sessions terminated properly",
                        "df1717", "https://i.ibb.co/P9gP1zM/error.png");
            } else {
                doSendMessage(platform, devicesInfo, incomingWebHookUrl, externalMsg,
                        "<font color=\"Orange\"><b>Warning!</b></font>. " +
                                "<br>Minimum number of devices found. " +
                                "<br>Test might run slower then expected.",
                        "d3df17", "https://i.ibb.co/1bBCJ5S/warn.png");
            }
        } else {
            doSendMessage(platform, devicesInfo, incomingWebHookUrl, externalMsg,
                    "<font color=\"MediumSeaGreen\"><b>All good!</b></font>. " +
                            "<br>Enough devices found." +
                            "<br> Test execution should run as expected.",
                    "008000", "https://i.ibb.co/g37WRCm/good.png");
        }
    }

    private void doSendMessage(String platform, List<DeviceInfo> devicesInfo, String incomingWebHookUrl, String externalMsg,
                               String statusMsg, String colourCode, String image) {

        List<Fact> facts = Collections.singletonList(new Fact(platform, devicesInfo));
        List<Section> sections = Collections.singletonList(new Section(image, facts));

        DeviceStatusMessage deviceStatusMessage = new DeviceStatusMessage(statusMsg, colourCode, externalMsg, sections);
        new IncomingWebHookApiSchema().getQuantitySchemaByPlatform(incomingWebHookUrl, deviceStatusMessage);
    }

    private int countIdleDevices(List<DeviceInfo> devicesInfo) {
        int count = 0;
        for (DeviceInfo deviceInfo : devicesInfo) {
            if (deviceInfo.deviceStatus.equalsIgnoreCase("idle")) {
                count++;
            }
        }
        return count;
    }

    private boolean pingHost(String host, int port) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port));
            return true;
        } catch (Exception e) {
            logger.error("Either timeout or unreachable or failed DNS lookup.");
            logger.trace(e.getMessage(), e);
            return false;
        }
    }

    private void doSleep(int waitMs) {
        try {
            sleep(waitMs);
        } catch (InterruptedException e) {
            logger.trace(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
    }
}
